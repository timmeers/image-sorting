﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace Image_Sorting
{
    static class Program
    {
        public static string ApplicationPath { get; set; }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FileInfo fi = new FileInfo(Assembly.GetExecutingAssembly().Location);
            ApplicationPath = fi.DirectoryName;

            Application.Run(new Form1());
        }
    }
}
