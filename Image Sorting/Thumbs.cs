﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Reflection;

namespace Image_Sorting
{
    internal class Thumbs
    {
        public static string GenerateThumbnail(FileInfo info, string tempName, int targetSize)
        {
            byte[] dataArray = ResizeImageFile(targetSize, info.FullName);

            var fileName = Path.Combine(Program.ApplicationPath, tempName);

            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                // Write the data to the file, byte by byte. 
                for (int i = 0; i < dataArray.Length; i++)
                {
                    fileStream.WriteByte(dataArray[i]);
                }

                // Set the stream position to the beginning of the file.
                fileStream.Seek(0, SeekOrigin.Begin);

                // Read and verify the data. 
                for (int i = 0; i < fileStream.Length; i++)
                {
                    if (dataArray[i] != fileStream.ReadByte())
                    {
                        return "";
                    }
                }
            }

            return fileName;
        }

        private static byte[] ResizeImageFile(int targetSize, string path)
        {

            using (System.Drawing.Image oldImage = System.Drawing.Image.FromFile(path)) //.FromStream(new MemoryStream(imageFile)))
            {
                Size newSize = CalculateDimensions(oldImage.Size, targetSize);

                //Change from Format64bppPArgb to Format32bppRgb
                using (Bitmap newImage = new Bitmap(newSize.Width, newSize.Height, PixelFormat.Format32bppRgb))//.Format24bppRgb))
                {
                    newImage.SetResolution(oldImage.HorizontalResolution, oldImage.VerticalResolution);
                    using (Graphics canvas = Graphics.FromImage(newImage))
                    {
                        canvas.SmoothingMode = SmoothingMode.AntiAlias;
                        canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        canvas.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        canvas.DrawImage(oldImage, new Rectangle(new Point(0, 0), newSize));
                        MemoryStream m = new MemoryStream();
                        newImage.Save(m, ImageFormat.Jpeg);
                        return m.GetBuffer();
                    }
                }

            }
        }

        private static Size CalculateDimensions(Size oldSize, int targetSize)
        {
            Size newSize = new Size();
            if (oldSize.Width > oldSize.Height)
            {
                newSize.Width = targetSize;
                newSize.Height = (int)(oldSize.Height * (float)targetSize / (float)oldSize.Width);
            }
            else
            {
                newSize.Width = (int)(oldSize.Width * (float)targetSize / (float)oldSize.Height);
                newSize.Height = targetSize;
            }
            return newSize;
        }
    }
}
