﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Image_Sorting
{
    public partial class EditImagePath : Form
    {
        private static string TempPath = "";

        public string FileListRow = "";
        public string FileDate = "";
        public string FilePath = "";
        public string MovingFilePath = "";

        public string NewFileDate = "";
        public string NewFilePath = "";

        public EditImagePath()
        {
            InitializeComponent();



        }

        public void LoadForm()
        {

            fileDate.Text = this.FileDate;
            filePath.Text = this.FilePath;
            newDate.Text = this.FileDate;
            newPath.Text = this.MovingFilePath;
        }

        private void EditImagePath_Load(object sender, EventArgs e)
        {
            FileInfo fi = new FileInfo(FilePath);

            if (ImageTypes.IsImageType(fi))
            {
                var tempFileName = "temp_" + fi.Name;
                TempPath = Thumbs.GenerateThumbnail(fi, tempFileName, display.Width);

                using (System.Drawing.Image image = System.Drawing.Image.FromFile(TempPath))
                {//.FromStream(new MemoryStream(imageFile)))
                    int imageHeight = image.Height;

                    if (imageHeight >= 500)
                        imageHeight = 500;

                    this.Height = Height + imageHeight;


                    display.ImageLocation = TempPath;
                    display.Height = imageHeight;
                }
                this.FormClosing += new FormClosingEventHandler(EditImagePath_FormClosing);
            }

            
        }

        void EditImagePath_FormClosing(object sender, FormClosingEventArgs e)
        {
            File.Delete(TempPath);
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1.FileList.Find(x => x.DestinationFile == NewFilePath);
        }


    }
}
