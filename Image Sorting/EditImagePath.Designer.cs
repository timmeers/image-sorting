﻿namespace Image_Sorting
{
    partial class EditImagePath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.newDate = new System.Windows.Forms.TextBox();
            this.fileDate = new System.Windows.Forms.Label();
            this.filePath = new System.Windows.Forms.Label();
            this.movingToPath = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.newPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.display = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.display)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "New Date";
            // 
            // newDate
            // 
            this.newDate.Location = new System.Drawing.Point(82, 85);
            this.newDate.Name = "newDate";
            this.newDate.Size = new System.Drawing.Size(265, 20);
            this.newDate.TabIndex = 1;
            // 
            // fileDate
            // 
            this.fileDate.AutoSize = true;
            this.fileDate.Location = new System.Drawing.Point(79, 24);
            this.fileDate.Name = "fileDate";
            this.fileDate.Size = new System.Drawing.Size(35, 13);
            this.fileDate.TabIndex = 2;
            this.fileDate.Text = "label2";
            // 
            // filePath
            // 
            this.filePath.AutoSize = true;
            this.filePath.Location = new System.Drawing.Point(79, 37);
            this.filePath.Name = "filePath";
            this.filePath.Size = new System.Drawing.Size(35, 13);
            this.filePath.TabIndex = 3;
            this.filePath.Text = "label3";
            // 
            // movingToPath
            // 
            this.movingToPath.AutoSize = true;
            this.movingToPath.Location = new System.Drawing.Point(79, 50);
            this.movingToPath.Name = "movingToPath";
            this.movingToPath.Size = new System.Drawing.Size(35, 13);
            this.movingToPath.TabIndex = 4;
            this.movingToPath.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "File Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "File Path";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Moving To";
            // 
            // newPath
            // 
            this.newPath.Location = new System.Drawing.Point(82, 111);
            this.newPath.Name = "newPath";
            this.newPath.Size = new System.Drawing.Size(265, 20);
            this.newPath.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "New Path";
            // 
            // display
            // 
            this.display.Location = new System.Drawing.Point(15, 137);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(328, 10);
            this.display.TabIndex = 10;
            this.display.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(355, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.updateToolStripMenuItem.Text = "Update";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // EditImagePath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 148);
            this.Controls.Add(this.display);
            this.Controls.Add(this.newPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.movingToPath);
            this.Controls.Add(this.filePath);
            this.Controls.Add(this.fileDate);
            this.Controls.Add(this.newDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EditImagePath";
            this.Text = "EditImagePath";
            this.Load += new System.EventHandler(this.EditImagePath_Load);
            ((System.ComponentModel.ISupportInitialize)(this.display)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox newDate;
        private System.Windows.Forms.Label fileDate;
        private System.Windows.Forms.Label filePath;
        private System.Windows.Forms.Label movingToPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox newPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox display;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
    }
}