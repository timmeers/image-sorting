﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Image_Sorting
{
    public class Sort
    {
        public string SourceFile { get; set; }
        public string DestinationFile { get; set; }
        public string FileName { get; set; }
        public int InError { get; set; }
        public DateTime FileDate { get; set; }

        public string[] SourceFolders { get; set; }
        public string[] SourceFiles { get; set; }

        public List<Sort> FileList { get; set; }



    }
}
