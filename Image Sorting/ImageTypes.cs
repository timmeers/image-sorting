﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Image_Sorting
{
    public static class ImageTypes
    {
        private static Dictionary<string, string> types;
        private static void LoadTypes()
        {
            types = new Dictionary<string, string>(100);

            // Images
            types.Add("jpg", "image/jpeg");
            types.Add("jpeg", "image/jpeg");
            types.Add("jpe", "image/jpeg");
            types.Add("gif", "image/gif");
            types.Add("png", "image/png");
            types.Add("bmp", "image/bmp");

        }

        /// <summary>
        /// Gets the list of the MIME Types.
        /// </summary>
        //public static Dictionary<string, string> Types
        //{
        //    get { return types; }
        //}
        private static Dictionary<string, string> Types
        {
            get
            {
                if (types == null)
                {
                    LoadTypes();
                }

                return types;
            }
        }

        public static bool IsImageType(FileInfo info)
        {
            bool ret = false;
            ret = Types.ContainsValue(GetMimeType(info.Extension));
            return ret;
        }

        private static string GetMimeType(string ext)
        {
            string mime = "";

            if (MimeTypes.Types.TryGetValue(ext.Remove(0, 1).ToLowerInvariant(), out mime))
                return mime;
            else
                return "application/octet-stream";

            //return "";
        }
    }
}
