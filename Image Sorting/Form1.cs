﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using Danielyan.Exif;


namespace Image_Sorting
{
    public partial class Form1 : Form
    {
        FolderBrowserDialog BrowseSource = new FolderBrowserDialog();
        FolderBrowserDialog BrowseDestination = new FolderBrowserDialog();
        public static List<Sort> FileList = new List<Sort>();

        public static HashSet<string> IncludePatternSelections = new HashSet<string>();

        public Form1()
        {
            InitializeComponent();

            source.Click += new EventHandler(source_Click);
            destination.Click += new EventHandler(destination_Click);
            BrowseSource.RootFolder = Environment.SpecialFolder.DesktopDirectory;
			filesList.DoubleClick += new EventHandler(filesList_DoubleClick);
            examplePath.Text = string.Empty;

            FileListContainer.AutoSize = true;
            //FileListContainer.

            filesList.Columns[1].Width = 225; //.AutoResizeColumn(1, ColumnHeaderAutoResizeStyle.ColumnContent);
            filesList.Columns[2].Width = 240; //.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.ColumnContent);
            
        }


        #region Simple button clicks
        private void source_Click(object sender, EventArgs e)
        {
            BrowseSource.RootFolder = System.Environment.SpecialFolder.MyComputer;
            BrowseSource.ShowDialog();

            if (BrowseSource.SelectedPath != "")
                source.Text = BrowseSource.SelectedPath;
        }

        /// <summary>
        /// Opens and sets the destination folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void destination_Click(object sender, EventArgs e)
        {
            BrowseDestination.RootFolder = System.Environment.SpecialFolder.MyComputer;
            BrowseDestination.ShowDialog();

            if (BrowseDestination.SelectedPath != "")
            {
                destination.Text = BrowseDestination.SelectedPath;
                BuildExamplePath();
            }
        }

        private void year_CheckedChanged(object sender, EventArgs e)
        {
            BuildExamplePath();
        }

        private void month_CheckedChanged(object sender, EventArgs e)
        {
            BuildExamplePath();
        }

        private void day_CheckedChanged(object sender, EventArgs e)
        {
            BuildExamplePath();
        }

        private void BuildExamplePath()
        {
            DateTime _dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            StringBuilder _sb = new StringBuilder();
            string _format = @"{0}\";

            if (destination.TextLength != 0)
            {
                _sb.AppendFormat(_format, destination.Text.Trim());

                if (year.Checked)
                    _sb.AppendFormat(_format, _dt.Year.ToString());

                if (month.Checked && monthAlpha.Checked)
					_sb.AppendFormat(_format, _dt.ToString("MMM"));
				else if (month.Checked && monthNumeric.Checked)
					_sb.AppendFormat(_format, _dt.Month.ToString());

                if (day.Checked)
                    _sb.AppendFormat(_format, _dt.Day.ToString());

                examplePath.Text = _sb.ToString();
            }
        }

        private void mobile_CheckedChanged(object sender, EventArgs e)
        {
            if (mobile.Checked)
            {
                ignore.Text += ".thumb,";
            }
            else
            {
                if (ignore.Text.Contains(".thumb,"))
                    ignore.Text = ignore.Text.Replace(".thumb,", "");
            }

			var drives = DriveInfo.GetDrives().Where(drive => drive.IsReady && drive.DriveType == DriveType.Removable);
        }

        private void helpIgnore_Click(object sender, EventArgs e)
        {

        }

        private void helpOptional_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Basics about where your images are. Assumptions will 
be made depending on your selection.", "Help: Options");

        }
        #endregion

        private string BuildPath(DateTime createdDate, string fileName)
        {
            DateTime _dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            StringBuilder _sb = new StringBuilder();
            string _format = @"{0}\";

            if (destination.TextLength != 0)
            {
                _sb.AppendFormat(_format, destination.Text.Trim());

                if (year.Checked)
                    _sb.AppendFormat(_format, createdDate.Year.ToString());

				if (month.Checked && monthAlpha.Checked)
					_sb.AppendFormat(_format, createdDate.ToString("MMM"));
				else if (month.Checked && monthNumeric.Checked)
					_sb.AppendFormat(_format, createdDate.ToString("MM"));

                if (day.Checked)
                    _sb.AppendFormat(_format, createdDate.ToString("dd"));

                _sb.Append(fileName);

                return _sb.ToString();
            }

            return "";
        }

        //private DateTime DateLastTaken(string path)
        //{
        //    Image myImage = Image.FromFile(path);
        //    PropertyItem[] propItems = myImage.PropertyItems;

        //    PropertyItem propItem = myImage.GetPropertyItem(36868);
        //    string dateTaken = Encoding.UTF8.GetString(propItem.Value) ;
        //    FileInfo _fi = new FileInfo(path);

        //    DateTime _dt;
        //    if (string.IsNullOrEmpty(dateTaken))
        //        _dt = _fi.LastWriteTime; 
        //    else
        //        _dt = DateTime.Parse(dateTaken);

        //    return _dt;
        //}

        private DateTime FileDate(FileInfo fi)
        {
            string _date = GetExifDate(fi.FullName);

            if (string.IsNullOrEmpty(_date))
                return fi.LastWriteTime;

            // The DateTimeOriginal exists so parse it and return

            return ParseDate(fi, _date);
        }

        private DateTime ParseDate(FileInfo fi, string date)
        {
            try
            {
                return DateTime.Parse(date);
            }
            catch
            {
                return fi.LastWriteTime;
            }
        }

        /// <summary>
        /// Returns the DateTimeOriginal if possible
        /// Returns empty if DateTimeOriginal not found
        /// Ugly, hacky, exception based garbage but it works for now.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string GetExifDate(string path)
        {
            try
            {
                var tags = new ExifTags(path);
                ExifTag original = tags["DateTimeOriginal"];
                return original.Value.ToString();
            }
            catch //(KeyNotFoundException)
            {
                //Console.WriteLine("Error: Tag not found");
                return string.Empty;
            }
        }

        private IEnumerable<string> IsIgnored(HashSet<string> files)
        {
            string[] _ignore = 
            {
                ""
            };

            if (ignore.Text.Trim().Contains(','))
            {
                _ignore = ignore.Text.Trim().Split(',');
            }

            List<string> _files = new List<string>();// = files.Contains(ignoredFolders);//.Remove(

            foreach (var item in _ignore)
            {
                if (item.Length >= 1)
                {
                    _files.AddRange(from x in files
                                    where x.Contains(item)
                                    select x);
                }
            }

            return _files;
        }

        private void preview_Click(object sender, EventArgs e)
        {
            if (filesList.Items.Count != 0)
            {
                filesList.Items.Clear();
                FileList.Clear();
            }

            if (!ignore.Text.Contains(','))
                ignore.Text = ignore.Text + ",";

            string[] _include = 
            {
                "*.*"
            };

            if (include.Text.Trim().Contains(','))
            {
                _include = include.Text.Trim().Split(',');
            }

            SearchOption _option = SearchOption.AllDirectories;
            if (topFolder.Checked)
                _option = SearchOption.TopDirectoryOnly;

            // Builds a set of all files in the selected directory
           // List<string> _list = Directory.EnumerateFiles(source.Text.Trim(),"*.*", _option).ToList();
            HashSet<string> _hs = new HashSet<string>(MyDirectory.GetFiles(source.Text.Trim(), _include, _option).ToList().AsParallel());

            // Builds another set of all files that are ignored based on the ignore rules selected
            HashSet<string> _ignored = new HashSet<string>();
            
            if (ignore.Text.Trim().Length >= 2)
                _ignored = new HashSet<string>(IsIgnored(_hs).ToList().AsParallel());

            // Loops each file in the set except the files being ignored
            foreach (var _file in _hs.Except(_ignored).AsParallel())
            {
                FileInfo _s = new FileInfo(_file);
                DateTime _sourceDate;

                if (_s.Name.EndsWith("jpg") || _s.Name.EndsWith("mpo"))
                    _sourceDate = FileDate(_s);
                else
                    _sourceDate = _s.LastWriteTime;

                FileInfo _d = new FileInfo(BuildPath(_sourceDate, _s.Name));

                FileList.Add(new Sort { SourceFile = _s.FullName,
                                        DestinationFile = _d.FullName,
                                        InError = 0,
                                        FileDate = _sourceDate});

                ListViewItem LVI = filesList.Items.Add(_sourceDate.ToString("MM/dd/yyyy hh:mm:ss"));
                LVI.SubItems.Add(_s.FullName);
                LVI.SubItems.Add(_d.FullName);

            }

            List<DateTime> dates = FileList.Select(x => x.FileDate).ToList();
            minDate.Text = dates.Min().ToString();
            maxDate.Text = dates.Max().ToString();
            //string _minFileDate = FileList.Select(
            
            sourceFileCount.Text = FileList.Count().ToString();
            process.Visible = true;

            filesList.Columns[1].Width = 225; //.AutoResizeColumn(1, ColumnHeaderAutoResizeStyle.ColumnContent);
            filesList.Columns[2].Width = 240; //.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.ColumnContent);
        }


        private void process_Click(object sender, EventArgs e)
        {
            foreach (var _file in FileList)
            {
                FileInfo _s = new FileInfo(_file.SourceFile);
                FileInfo _d = new FileInfo(_file.DestinationFile);
                string _uniqueName = _d.FullName;
                DirectoryInfo _di = new DirectoryInfo(_d.DirectoryName);
                
                if (!_di.Exists)
                    _di.Create();

                bool keepOriginal = overwrite.Checked;

                if (_d.Exists && unique.Checked)
                {
                    string _path = _d.DirectoryName;
                    string _name = DateTime.Now.ToString("mm.ss") + _d.Name;

                    _uniqueName = Path.Combine(_path, _name);
                }

                if (copyFile.Checked)
                {
                    File.Copy(_s.FullName, _uniqueName, overwrite.Checked);
                }
                else if (moveFile.Checked)
                {
                    File.Copy(_s.FullName, _uniqueName, overwrite.Checked);
                    _s.Delete();
                }

            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            //MessageBoxButtons _yn = MessageBoxButtons.YesNo;

            //MessageBox.Show("The 
        }

        private void allFolders_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ProcessPicture(string source, string destination)
        {

        }

        private void helpInclude_Click(object sender, EventArgs e)
        {
            IncludePatternOptions _ipo = new IncludePatternOptions();
            _ipo.ShowDialog();
            
            IncludePatternSelections = _ipo.IncludePattern;

            include.Text = String.Join(",", _ipo.IncludePattern.ToList());
        }

        public static void ProcessIncludeOptions()
        {
            
        }

		private void monthNumeric_CheckedChanged(object sender, EventArgs e)
		{
			BuildExamplePath();
		}

		private void monthAlpha_CheckedChanged(object sender, EventArgs e)
		{
			BuildExamplePath();
		}



		private void filesList_DoubleClick(System.Object sender, System.EventArgs e)
		{
			if (filesList.SelectedItems.Count == 1)
			{
                EditImagePath _eip = new EditImagePath();
                _eip.FileDate = filesList.SelectedItems[0].SubItems[0].Text;
                _eip.FilePath = filesList.SelectedItems[0].SubItems[1].Text;
                _eip.MovingFilePath = filesList.SelectedItems[0].SubItems[2].Text;
                _eip.FileListRow = filesList.SelectedIndices.ToString(); //FileList.Select(x => x.

                _eip.LoadForm();
                _eip.Show();
                //MessageBox.Show(filesList.SelectedItems[0].SubItems[0].Text + "\r\n" + filesList.SelectedItems[0].SubItems[1].Text + "\r\n" + filesList.SelectedItems[0].SubItems[2].Text);
				//System.Diagnostics.Process.Start(filesList.SelectedItems[0].SubItems[1].Text);
			}

		}

    }
}
