﻿namespace Image_Sorting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.source = new System.Windows.Forms.TextBox();
            this.destination = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.year = new System.Windows.Forms.CheckBox();
            this.month = new System.Windows.Forms.CheckBox();
            this.day = new System.Windows.Forms.CheckBox();
            this.examplePath = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.sourceFolderCount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sourceFileCount = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.FileListContainer = new System.Windows.Forms.Panel();
            this.filesList = new System.Windows.Forms.ListView();
            this.created = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sour = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dest = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.process = new System.Windows.Forms.Button();
            this.preview = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.ignore = new System.Windows.Forms.TextBox();
            this.detailsContainer = new System.Windows.Forms.Panel();
            this.overwrite = new System.Windows.Forms.RadioButton();
            this.unique = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.copyFile = new System.Windows.Forms.RadioButton();
            this.moveFile = new System.Windows.Forms.RadioButton();
            this.include = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.allFolders = new System.Windows.Forms.RadioButton();
            this.topFolder = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.helpIgnore = new System.Windows.Forms.Button();
            this.helpInclude = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.monthAlpha = new System.Windows.Forms.RadioButton();
            this.monthNumeric = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mobile = new System.Windows.Forms.RadioButton();
            this.arbitrary = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.helpOptional = new System.Windows.Forms.Button();
            this.minDate = new System.Windows.Forms.Label();
            this.maxDate = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.FileListContainer.SuspendLayout();
            this.detailsContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // source
            // 
            this.source.Location = new System.Drawing.Point(95, 19);
            this.source.Name = "source";
            this.source.Size = new System.Drawing.Size(224, 20);
            this.source.TabIndex = 1;
            // 
            // destination
            // 
            this.destination.Location = new System.Drawing.Point(95, 45);
            this.destination.Name = "destination";
            this.destination.Size = new System.Drawing.Size(224, 20);
            this.destination.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Source Root";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(896, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveOptionsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveOptionsToolStripMenuItem
            // 
            this.saveOptionsToolStripMenuItem.Name = "saveOptionsToolStripMenuItem";
            this.saveOptionsToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.saveOptionsToolStripMenuItem.Text = "Save Options";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Destination Root";
            // 
            // year
            // 
            this.year.AutoSize = true;
            this.year.Checked = true;
            this.year.CheckState = System.Windows.Forms.CheckState.Checked;
            this.year.Location = new System.Drawing.Point(52, 21);
            this.year.Name = "year";
            this.year.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.year.Size = new System.Drawing.Size(48, 17);
            this.year.TabIndex = 6;
            this.year.Text = "Year";
            this.year.UseVisualStyleBackColor = true;
            this.year.CheckedChanged += new System.EventHandler(this.year_CheckedChanged);
            // 
            // month
            // 
            this.month.AutoSize = true;
            this.month.Checked = true;
            this.month.CheckState = System.Windows.Forms.CheckState.Checked;
            this.month.Location = new System.Drawing.Point(44, 43);
            this.month.Name = "month";
            this.month.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.month.Size = new System.Drawing.Size(56, 17);
            this.month.TabIndex = 7;
            this.month.Text = "Month";
            this.month.UseVisualStyleBackColor = true;
            this.month.CheckedChanged += new System.EventHandler(this.month_CheckedChanged);
            // 
            // day
            // 
            this.day.AutoSize = true;
            this.day.Checked = true;
            this.day.CheckState = System.Windows.Forms.CheckState.Checked;
            this.day.Location = new System.Drawing.Point(55, 66);
            this.day.Name = "day";
            this.day.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.day.Size = new System.Drawing.Size(45, 17);
            this.day.TabIndex = 8;
            this.day.Text = "Day";
            this.day.UseVisualStyleBackColor = true;
            this.day.CheckedChanged += new System.EventHandler(this.day_CheckedChanged);
            // 
            // examplePath
            // 
            this.examplePath.AutoSize = true;
            this.examplePath.Location = new System.Drawing.Point(83, 135);
            this.examplePath.Name = "examplePath";
            this.examplePath.Size = new System.Drawing.Size(22, 13);
            this.examplePath.TabIndex = 9;
            this.examplePath.Text = "C:\\";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Example Path";
            // 
            // sourceFolderCount
            // 
            this.sourceFolderCount.AutoSize = true;
            this.sourceFolderCount.Location = new System.Drawing.Point(111, 556);
            this.sourceFolderCount.Name = "sourceFolderCount";
            this.sourceFolderCount.Size = new System.Drawing.Size(13, 13);
            this.sourceFolderCount.TabIndex = 11;
            this.sourceFolderCount.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 556);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Total Source Folders";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 580);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Total Source Files";
            // 
            // sourceFileCount
            // 
            this.sourceFileCount.AutoSize = true;
            this.sourceFileCount.Location = new System.Drawing.Point(111, 580);
            this.sourceFileCount.Name = "sourceFileCount";
            this.sourceFileCount.Size = new System.Drawing.Size(13, 13);
            this.sourceFileCount.TabIndex = 14;
            this.sourceFileCount.Text = "0";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 630);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(896, 22);
            this.statusStrip1.TabIndex = 16;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // FileListContainer
            // 
            this.FileListContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileListContainer.BackColor = System.Drawing.SystemColors.Control;
            this.FileListContainer.Controls.Add(this.filesList);
            this.FileListContainer.Location = new System.Drawing.Point(335, 28);
            this.FileListContainer.Name = "FileListContainer";
            this.FileListContainer.Size = new System.Drawing.Size(561, 599);
            this.FileListContainer.TabIndex = 15;
            // 
            // filesList
            // 
            this.filesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.created,
            this.sour,
            this.dest});
            this.filesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filesList.Location = new System.Drawing.Point(0, 0);
            this.filesList.Name = "filesList";
            this.filesList.Size = new System.Drawing.Size(561, 599);
            this.filesList.TabIndex = 0;
            this.filesList.UseCompatibleStateImageBehavior = false;
            this.filesList.View = System.Windows.Forms.View.Details;
            // 
            // created
            // 
            this.created.Text = "CreatedDate";
            // 
            // sour
            // 
            this.sour.Text = "Source";
            // 
            // dest
            // 
            this.dest.Text = "Destination";
            this.dest.Width = 240;
            // 
            // process
            // 
            this.process.Location = new System.Drawing.Point(245, 604);
            this.process.Name = "process";
            this.process.Size = new System.Drawing.Size(75, 23);
            this.process.TabIndex = 18;
            this.process.Text = "Process";
            this.process.UseVisualStyleBackColor = true;
            this.process.Visible = false;
            this.process.Click += new System.EventHandler(this.process_Click);
            // 
            // preview
            // 
            this.preview.Location = new System.Drawing.Point(15, 604);
            this.preview.Name = "preview";
            this.preview.Size = new System.Drawing.Size(75, 23);
            this.preview.TabIndex = 19;
            this.preview.Text = "Preview";
            this.preview.UseVisualStyleBackColor = true;
            this.preview.Click += new System.EventHandler(this.preview_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Ignore Pattern";
            // 
            // ignore
            // 
            this.ignore.Location = new System.Drawing.Point(95, 110);
            this.ignore.Name = "ignore";
            this.ignore.Size = new System.Drawing.Size(203, 20);
            this.ignore.TabIndex = 22;
            // 
            // detailsContainer
            // 
            this.detailsContainer.Controls.Add(this.overwrite);
            this.detailsContainer.Controls.Add(this.unique);
            this.detailsContainer.Location = new System.Drawing.Point(109, 48);
            this.detailsContainer.Name = "detailsContainer";
            this.detailsContainer.Size = new System.Drawing.Size(209, 19);
            this.detailsContainer.TabIndex = 26;
            // 
            // overwrite
            // 
            this.overwrite.AutoSize = true;
            this.overwrite.Location = new System.Drawing.Point(98, 1);
            this.overwrite.Name = "overwrite";
            this.overwrite.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.overwrite.Size = new System.Drawing.Size(108, 17);
            this.overwrite.TabIndex = 1;
            this.overwrite.TabStop = true;
            this.overwrite.Text = "Overwrite Original";
            this.overwrite.UseVisualStyleBackColor = true;
            // 
            // unique
            // 
            this.unique.AutoSize = true;
            this.unique.Checked = true;
            this.unique.Location = new System.Drawing.Point(6, 1);
            this.unique.Name = "unique";
            this.unique.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.unique.Size = new System.Drawing.Size(89, 17);
            this.unique.TabIndex = 0;
            this.unique.TabStop = true;
            this.unique.Text = "Make Unique";
            this.unique.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.copyFile);
            this.panel1.Controls.Add(this.moveFile);
            this.panel1.Location = new System.Drawing.Point(109, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(209, 21);
            this.panel1.TabIndex = 27;
            // 
            // copyFile
            // 
            this.copyFile.AutoSize = true;
            this.copyFile.Checked = true;
            this.copyFile.Location = new System.Drawing.Point(27, 3);
            this.copyFile.Name = "copyFile";
            this.copyFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.copyFile.Size = new System.Drawing.Size(68, 17);
            this.copyFile.TabIndex = 3;
            this.copyFile.TabStop = true;
            this.copyFile.Text = "Copy File";
            this.copyFile.UseVisualStyleBackColor = true;
            // 
            // moveFile
            // 
            this.moveFile.AutoSize = true;
            this.moveFile.Location = new System.Drawing.Point(135, 3);
            this.moveFile.Name = "moveFile";
            this.moveFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.moveFile.Size = new System.Drawing.Size(71, 17);
            this.moveFile.TabIndex = 2;
            this.moveFile.Text = "Move File";
            this.moveFile.UseVisualStyleBackColor = true;
            // 
            // include
            // 
            this.include.Location = new System.Drawing.Point(95, 14);
            this.include.Name = "include";
            this.include.Size = new System.Drawing.Size(203, 20);
            this.include.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Include Pattern";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.allFolders);
            this.panel2.Controls.Add(this.topFolder);
            this.panel2.Location = new System.Drawing.Point(7, 73);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(308, 22);
            this.panel2.TabIndex = 30;
            // 
            // allFolders
            // 
            this.allFolders.AutoSize = true;
            this.allFolders.Checked = true;
            this.allFolders.Location = new System.Drawing.Point(217, 3);
            this.allFolders.Name = "allFolders";
            this.allFolders.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.allFolders.Size = new System.Drawing.Size(73, 17);
            this.allFolders.TabIndex = 5;
            this.allFolders.TabStop = true;
            this.allFolders.Text = "All Folders";
            this.allFolders.UseVisualStyleBackColor = true;
            this.allFolders.CheckedChanged += new System.EventHandler(this.allFolders_CheckedChanged);
            // 
            // topFolder
            // 
            this.topFolder.AutoSize = true;
            this.topFolder.Location = new System.Drawing.Point(88, 3);
            this.topFolder.Name = "topFolder";
            this.topFolder.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.topFolder.Size = new System.Drawing.Size(123, 17);
            this.topFolder.TabIndex = 4;
            this.topFolder.Text = "Only Selected Folder";
            this.topFolder.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.helpIgnore);
            this.groupBox1.Controls.Add(this.helpInclude);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.include);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.ignore);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(7, 161);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(322, 156);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Options";
            // 
            // helpIgnore
            // 
            this.helpIgnore.Location = new System.Drawing.Point(304, 108);
            this.helpIgnore.Name = "helpIgnore";
            this.helpIgnore.Size = new System.Drawing.Size(15, 23);
            this.helpIgnore.TabIndex = 34;
            this.helpIgnore.Text = "?";
            this.helpIgnore.UseVisualStyleBackColor = true;
            this.helpIgnore.Click += new System.EventHandler(this.helpIgnore_Click);
            // 
            // helpInclude
            // 
            this.helpInclude.Location = new System.Drawing.Point(304, 12);
            this.helpInclude.Name = "helpInclude";
            this.helpInclude.Size = new System.Drawing.Size(15, 23);
            this.helpInclude.TabIndex = 33;
            this.helpInclude.Text = "?";
            this.helpInclude.UseVisualStyleBackColor = true;
            this.helpInclude.Click += new System.EventHandler(this.helpInclude_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(37, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(282, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Exclude files by extension. Separate by commas: *.jpg,*.avi";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(51, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(268, 26);
            this.label9.TabIndex = 31;
            this.label9.Text = "Inclue files by white list. Separate by commas: *.jpg,*.avi\r\nDefault *.* will inc" +
    "lude all files";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.detailsContainer);
            this.groupBox2.Controls.Add(this.year);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.month);
            this.groupBox2.Controls.Add(this.day);
            this.groupBox2.Controls.Add(this.examplePath);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(7, 323);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(322, 151);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Destination Options";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.monthAlpha);
            this.panel3.Controls.Add(this.monthNumeric);
            this.panel3.Location = new System.Drawing.Point(110, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(209, 43);
            this.panel3.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Month Format";
            // 
            // monthAlpha
            // 
            this.monthAlpha.AutoSize = true;
            this.monthAlpha.Location = new System.Drawing.Point(92, 23);
            this.monthAlpha.Name = "monthAlpha";
            this.monthAlpha.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.monthAlpha.Size = new System.Drawing.Size(113, 17);
            this.monthAlpha.TabIndex = 1;
            this.monthAlpha.TabStop = true;
            this.monthAlpha.Text = "Abbrevated Month";
            this.monthAlpha.UseVisualStyleBackColor = true;
            this.monthAlpha.CheckedChanged += new System.EventHandler(this.monthAlpha_CheckedChanged);
            // 
            // monthNumeric
            // 
            this.monthNumeric.AutoSize = true;
            this.monthNumeric.Checked = true;
            this.monthNumeric.Location = new System.Drawing.Point(108, 3);
            this.monthNumeric.Name = "monthNumeric";
            this.monthNumeric.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.monthNumeric.Size = new System.Drawing.Size(97, 17);
            this.monthNumeric.TabIndex = 0;
            this.monthNumeric.TabStop = true;
            this.monthNumeric.Text = "Numeric Month";
            this.monthNumeric.UseVisualStyleBackColor = true;
            this.monthNumeric.CheckedChanged += new System.EventHandler(this.monthNumeric_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Path";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.source);
            this.groupBox3.Controls.Add(this.destination);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(7, 81);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(322, 74);
            this.groupBox3.TabIndex = 33;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Path Selections";
            // 
            // mobile
            // 
            this.mobile.AutoSize = true;
            this.mobile.Location = new System.Drawing.Point(109, 22);
            this.mobile.Name = "mobile";
            this.mobile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.mobile.Size = new System.Drawing.Size(93, 17);
            this.mobile.TabIndex = 4;
            this.mobile.Text = "Mobile Device";
            this.mobile.UseVisualStyleBackColor = true;
            this.mobile.CheckedChanged += new System.EventHandler(this.mobile_CheckedChanged);
            // 
            // arbitrary
            // 
            this.arbitrary.AutoSize = true;
            this.arbitrary.Location = new System.Drawing.Point(7, 22);
            this.arbitrary.Name = "arbitrary";
            this.arbitrary.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.arbitrary.Size = new System.Drawing.Size(97, 17);
            this.arbitrary.TabIndex = 6;
            this.arbitrary.Text = "Random Folder";
            this.arbitrary.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.helpOptional);
            this.groupBox4.Controls.Add(this.mobile);
            this.groupBox4.Controls.Add(this.arbitrary);
            this.groupBox4.Location = new System.Drawing.Point(7, 28);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(322, 47);
            this.groupBox4.TabIndex = 34;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Optional Information";
            // 
            // helpOptional
            // 
            this.helpOptional.Location = new System.Drawing.Point(301, 19);
            this.helpOptional.Name = "helpOptional";
            this.helpOptional.Size = new System.Drawing.Size(15, 23);
            this.helpOptional.TabIndex = 35;
            this.helpOptional.Text = "?";
            this.helpOptional.UseVisualStyleBackColor = true;
            this.helpOptional.Click += new System.EventHandler(this.helpOptional_Click);
            // 
            // minDate
            // 
            this.minDate.AutoSize = true;
            this.minDate.Location = new System.Drawing.Point(117, 481);
            this.minDate.Name = "minDate";
            this.minDate.Size = new System.Drawing.Size(46, 13);
            this.minDate.TabIndex = 35;
            this.minDate.Text = "minDate";
            // 
            // maxDate
            // 
            this.maxDate.AutoSize = true;
            this.maxDate.Location = new System.Drawing.Point(117, 498);
            this.maxDate.Name = "maxDate";
            this.maxDate.Size = new System.Drawing.Size(49, 13);
            this.maxDate.TabIndex = 36;
            this.maxDate.Text = "maxDate";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 652);
            this.Controls.Add(this.maxDate);
            this.Controls.Add(this.minDate);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.preview);
            this.Controls.Add(this.process);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.FileListContainer);
            this.Controls.Add(this.sourceFileCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.sourceFolderCount);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.FileListContainer.ResumeLayout(false);
            this.detailsContainer.ResumeLayout(false);
            this.detailsContainer.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox source;
        private System.Windows.Forms.TextBox destination;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox year;
        private System.Windows.Forms.CheckBox month;
        private System.Windows.Forms.CheckBox day;
        private System.Windows.Forms.Label examplePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label sourceFolderCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label sourceFileCount;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel FileListContainer;
        private System.Windows.Forms.ListView filesList;
        private System.Windows.Forms.Button process;
        private System.Windows.Forms.Button preview;
        private System.Windows.Forms.ColumnHeader created;
        private System.Windows.Forms.ColumnHeader sour;
        private System.Windows.Forms.ColumnHeader dest;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ignore;
        private System.Windows.Forms.Panel detailsContainer;
        private System.Windows.Forms.RadioButton overwrite;
        private System.Windows.Forms.RadioButton unique;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton moveFile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton allFolders;
        private System.Windows.Forms.RadioButton topFolder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton copyFile;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button helpInclude;
        private System.Windows.Forms.RadioButton mobile;
        public System.Windows.Forms.RadioButton arbitrary;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.TextBox include;
        private System.Windows.Forms.Button helpIgnore;
        private System.Windows.Forms.Button helpOptional;
		private System.Windows.Forms.ToolStripMenuItem saveOptionsToolStripMenuItem;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.RadioButton monthAlpha;
		private System.Windows.Forms.RadioButton monthNumeric;
		private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label minDate;
        private System.Windows.Forms.Label maxDate;
    }
}

