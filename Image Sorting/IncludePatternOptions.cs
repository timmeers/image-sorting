﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Image_Sorting
{
    public partial class IncludePatternOptions : Form
    {
        public HashSet<string> IncludePattern = new HashSet<string>();

        public IncludePatternOptions()
        {
            InitializeComponent();
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (common.Checked)
            {
                jpg.Checked = true;
                mp4.Checked = true;
                avi.Checked = true;
				mpo.Checked = true;
            }
            else
            {
                jpg.Checked = false;
                mp4.Checked = false;
                avi.Checked = false;
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            
            //IncludePattern.Clear();

        //private System.Windows.Forms.CheckBox jpg;
        //private System.Windows.Forms.CheckBox wmv;
        //private System.Windows.Forms.CheckBox mp4;
        //private System.Windows.Forms.CheckBox mpeg;
        //private System.Windows.Forms.CheckBox avi;
        //private System.Windows.Forms.CheckBox web;


            if (jpg.Checked)
                IncludePattern.Add("*.jpg,*.jpeg");

            if (web.Checked)
                IncludePattern.Add("*.gif,*.png");


            if (wmv.Checked)
                IncludePattern.Add("*.wmv");

            if (mp4.Checked)
                IncludePattern.Add("*.mp4");
            
            if (mpeg.Checked)
                IncludePattern.Add("*.mpg");
        
            if (avi.Checked)
                IncludePattern.Add("*.avi");

			if (mpo.Checked)
				IncludePattern.Add("*.mpo");

            this.Close();
        }


    }
}
