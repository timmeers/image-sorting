﻿namespace Image_Sorting
{
    partial class IncludePatternOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.jpg = new System.Windows.Forms.CheckBox();
			this.wmv = new System.Windows.Forms.CheckBox();
			this.mp4 = new System.Windows.Forms.CheckBox();
			this.mpeg = new System.Windows.Forms.CheckBox();
			this.avi = new System.Windows.Forms.CheckBox();
			this.web = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.save = new System.Windows.Forms.Button();
			this.common = new System.Windows.Forms.CheckBox();
			this.mpo = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(155, 26);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select the type of files you wish\r\nto copy from your device";
			// 
			// jpg
			// 
			this.jpg.AutoSize = true;
			this.jpg.Location = new System.Drawing.Point(32, 93);
			this.jpg.Name = "jpg";
			this.jpg.Size = new System.Drawing.Size(66, 17);
			this.jpg.TabIndex = 1;
			this.jpg.Text = "jpg, jpeg";
			this.jpg.UseVisualStyleBackColor = true;
			// 
			// wmv
			// 
			this.wmv.AutoSize = true;
			this.wmv.Location = new System.Drawing.Point(32, 246);
			this.wmv.Name = "wmv";
			this.wmv.Size = new System.Drawing.Size(53, 17);
			this.wmv.TabIndex = 2;
			this.wmv.Text = "WMV";
			this.wmv.UseVisualStyleBackColor = true;
			// 
			// mp4
			// 
			this.mp4.AutoSize = true;
			this.mp4.Location = new System.Drawing.Point(32, 223);
			this.mp4.Name = "mp4";
			this.mp4.Size = new System.Drawing.Size(47, 17);
			this.mp4.TabIndex = 3;
			this.mp4.Text = "Mp4";
			this.mp4.UseVisualStyleBackColor = true;
			// 
			// mpeg
			// 
			this.mpeg.AutoSize = true;
			this.mpeg.Location = new System.Drawing.Point(32, 200);
			this.mpeg.Name = "mpeg";
			this.mpeg.Size = new System.Drawing.Size(57, 17);
			this.mpeg.TabIndex = 4;
			this.mpeg.Text = "MPEG";
			this.mpeg.UseVisualStyleBackColor = true;
			// 
			// avi
			// 
			this.avi.AutoSize = true;
			this.avi.Location = new System.Drawing.Point(32, 177);
			this.avi.Name = "avi";
			this.avi.Size = new System.Drawing.Size(43, 17);
			this.avi.TabIndex = 5;
			this.avi.Text = "AVI";
			this.avi.UseVisualStyleBackColor = true;
			// 
			// web
			// 
			this.web.AutoSize = true;
			this.web.Location = new System.Drawing.Point(32, 116);
			this.web.Name = "web";
			this.web.Size = new System.Drawing.Size(61, 17);
			this.web.TabIndex = 6;
			this.web.Text = "gif, png";
			this.web.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 161);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(39, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Videos";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 77);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Images";
			// 
			// save
			// 
			this.save.Location = new System.Drawing.Point(38, 280);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(87, 23);
			this.save.TabIndex = 9;
			this.save.Text = "Save Options";
			this.save.UseVisualStyleBackColor = true;
			this.save.Click += new System.EventHandler(this.save_Click);
			// 
			// common
			// 
			this.common.AutoSize = true;
			this.common.Location = new System.Drawing.Point(12, 57);
			this.common.Name = "common";
			this.common.Size = new System.Drawing.Size(131, 17);
			this.common.TabIndex = 10;
			this.common.Text = "Common Media Types";
			this.common.UseVisualStyleBackColor = true;
			this.common.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
			// 
			// mpo
			// 
			this.mpo.AutoSize = true;
			this.mpo.Location = new System.Drawing.Point(32, 139);
			this.mpo.Name = "mpo";
			this.mpo.Size = new System.Drawing.Size(69, 17);
			this.mpo.TabIndex = 11;
			this.mpo.Text = "mpo (3D)";
			this.mpo.UseVisualStyleBackColor = true;
			// 
			// IncludePatternOptions
			// 
			this.AcceptButton = this.save;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(165, 315);
			this.Controls.Add(this.mpo);
			this.Controls.Add(this.common);
			this.Controls.Add(this.save);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.web);
			this.Controls.Add(this.avi);
			this.Controls.Add(this.mpeg);
			this.Controls.Add(this.mp4);
			this.Controls.Add(this.wmv);
			this.Controls.Add(this.jpg);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "IncludePatternOptions";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Patterns";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox jpg;
        private System.Windows.Forms.CheckBox wmv;
        private System.Windows.Forms.CheckBox mp4;
        private System.Windows.Forms.CheckBox mpeg;
        private System.Windows.Forms.CheckBox avi;
        private System.Windows.Forms.CheckBox web;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.CheckBox common;
		private System.Windows.Forms.CheckBox mpo;
    }
}